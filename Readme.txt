To get vector of interpolated points use:
    L_v(xi,fi,x0,xe,step)
    vectort<T>xi - vector of x values, for which f(x) is known
    vector<T>fi - vector of known f(x) values, where  fi[i]=f(xi[i])
    T x0 - begining of the compartment in which f(x) is interpolated
    T xe - end of the compartment in which f(x) is interpolated
    T step - step for interpolating points in <x0,xe>
    
To get interpolated value in point x0 use:
    Lagrange(x0, xi, fi)
    vectort<T>xi - vector of x values, for which f(x) is known
    vector<T>fi - vector of known f(x) values, where  fi[i]=f(xi[i])